#include "complejo.h"
#include <iostream>

using namespace std;

int main (int argc, char* const argv[]) {

    Complejo z1, z2, z3(2, 3);

    cout << "Ingresar z1: ";
    z1.ingresar();

    cout << "Ingresar z2: ";
    z2.ingresar();

    cout << "z3: ";
    z3.mostrar();

    cout << "z1: ";
    z1.mostrar();

    cout << "z2: ";
    z2.mostrar();
    cout << z1.toString() <<" " << z2.toString () <<" " << z3.toString() << endl;

    return 0;
}
