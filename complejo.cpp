#include "complejo.h"
#include <sstream>

using namespace std;

Complejo::Complejo()
{
    real = 0;
    imag = 0;
}

Complejo::Complejo(int real, int imag)
{
    this->setReal(real);
    this->setImag(imag);
}

void Complejo::ingresar()
{
    int x;
    cout << "Ingrese parte real: ";
    cin >> x;
    this->setReal(x);
    cout << "Ingrese parte imag: ";
    cin >> x;
    this->setImag(x);
}

void Complejo::mostrar()
{
    cout << "(" << this->getReal() << ", " << this->getImag() << ")" << endl;
}

void Complejo::setReal(int real)
{
    this->real = real;
}

int Complejo::getReal()
{
    return this->real;
}

void Complejo::setImag(int imag)
{
    this->imag = imag;
}

int Complejo::getImag()
{
    return this->imag;
}
string Complejo::toString()
{
    stringstream ss;

    ss<< this->real<< "," <<this->imag;

    return ss.str();

}
