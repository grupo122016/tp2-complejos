#include <string>
#include <iostream>
#ifndef COMPLEJO_H
#define COMPLEJO_H
using namespace std;
class Complejo
{
    public:
        Complejo();
        Complejo(int, int);

        void ingresar();
        void mostrar();

        void setReal(int);
        int getReal();
        void setImag(int);
        int getImag();
        string toString();
    protected:
    private:
        int real;
        int imag;
};

#endif // COMPLEJO_H
